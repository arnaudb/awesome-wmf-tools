# Awesome Wmf Tools

After collective idea on [#wikimedia-sre-foundations](https://wm-bot.wmflabs.org/libera_logs/%23wikimedia-sre-foundations/20240703.txt), this repo is here to help us gather our favorite tooling in a comprehensive list. To simplify table maintenance, you can use [tablesgenerator](https://www.tablesgenerator.com/markdown_tables#) and paste table data from this file to update it.

# Favorite tools presentation

https://etherpad.wikimedia.org/p/dpe-favorite-tools

# System monitoring

| name   | url                                    | comment                                                              |
|--------|----------------------------------------|----------------------------------------------------------------------|
| btop   | https://github.com/aristocratos/btop   |                                                                      |
| bpytop | https://github.com/aristocratos/bpytop |                                                                      |
| viddy  | https://github.com/sachaos/viddy       | [demo](https://github.com/sachaos/viddy/blob/master/images/demo.gif) |


# Network

| name  | url                          | comment |
|-------|------------------------------|---------|
| gping | https://github.com/orf/gping |         |

# Misc

| name  | url                                 | comment |
|-------|-------------------------------------|---------|
| gitui | https://github.com/extrawurst/gitui |         |
